app.controller('UserIndexController', ['$scope', '$timeout', '$http', '$uibModal', function ($scope, $timeout, $http, $modal) {
	
	
	const ctrl = this;
	
	ctrl.tabsConfig = [
		{
			loading: false,
			reload: async function () {
				ctrl.tabsConfig[0].loading = true;
				
				//-- do the ajax thing
				let postResult = await $scope.global.utils.post(`${$scope.global.app.config.resourceServer}/api/rpc`, {
					name: 'get_user_info',
					params: {}
				});
				
				$scope.$apply(function(){
					ctrl.tabsConfig[0].loading = false;
					if (!postResult.success){
						alert($scope.global.utils.errors[postResult.error.errorName]);
					}
					else{
						ctrl.tabsConfig[0].userInfo = postResult.result.user;
					}
				});
			},
		},
		{
			loading: false,
			reload: async function () {
			
			},
		},
		{
			loading: false,
			reload: async function () {
			
			},
		}
	];
	
	ctrl.selectTab = function (index) {
		ctrl.selectedTab = index;
		ctrl.tabsConfig[index].reload();
	};
	
	ctrl.init = function () {
		ctrl.selectTab(0);
	};
	
	ctrl.editUserInfo = function(){
		$modal.open({
			templateUrl: 'edit-user-info-dialog',
			controller: 'EditUserInfoDialogController',
			scope: $scope,
			resolve: {
				/*
				* Pass controller object to dialog by populating options object
				* */
				options: function(){
					return {
						firstName: ctrl.tabsConfig[0].userInfo.firstName,
						lastName: ctrl.tabsConfig[0].userInfo.lastName
					};
				}
			}
		}).result.then(
			function(response){
				//-- process response object to update controller
				ctrl.tabsConfig[0].userInfo.firstName = response.firstName;
				ctrl.tabsConfig[0].userInfo.lastName = response.lastName;
			},
			function(){
				//-- dialog dismissed, do nothing
			}
		);
	}
	
}]);
