let app = document.app = angular.module('app', ['ngRoute', 'ngSanitize', 'ngStorage', 'ui.bootstrap', 'xeditable', 'cleave.js']);

/*
* This config is to set the default theme for Angular xeditable library.
* */
app.run(function (editableOptions) {
	editableOptions.theme = 'bs3';
});

/*
* New angular comes with hashPrefix change. This setting set it back to normal anchor symbol '#'.
* */
app.config(['$locationProvider', function ($locationProvider) {
	$locationProvider.hashPrefix('');
}]);

/*
* New angular removed XMLHttpRequest header. However, we need this header to distinguish between json RPC requests and normal requests.
* This config adds 'XMLHttpRequest' header to every $http request.
* */
app.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}]);

/*
* CONFIG SPA routes
* Map each SPA route to a view route.
* */
app.config(function ($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: '/views/index',
		})
		.when('/page1', {
			templateUrl: '/views/home/page1',
		})
		.when('/page2', {
			templateUrl: '/views/home/page2',
		})
		.when('/more/page1', {
			templateUrl: '/views/home/more/page1',
		})
		.when('/more/page2', {
			templateUrl: '/views/home/more/page2',
		})
		.when('/user/index', {
			templateUrl: '/views/user/index',
		});
});

/*
* GlobalController is the top-covered controller that runs some global init logic before other controllers take control.
* */
app.controller('GlobalController', ['$scope', '$http', '$localStorage', function ($scope, $http, $localStorage) {
	
	
	/*
	* The 'initData' object is parsed from the app.pug view, and its content depends on how we populate the view.
	* This boilerplate suggests that only error translations and some basic configs should be populated to the app main file.
	* */
	$scope.init = function (initData) {
		
		/*
		* $localStorage is either empty object {} or last-modified $scope.global object.
		* An global.app object holds any thing relevant to the app state (not the user).
		* */
		$scope.global = $localStorage;
		$scope.global.app = $scope.global.app || {};
		$scope.global.app.config = initData.config;
		$scope.global.app.data = $scope.global.app.data || {};
		
		/*
		* If the language (locale) is not set then the default language is used and save to cookie.
		* The default language is configured server side in 'config/i18n.js'
		* */
		if (!Cookies.get('lang')) {
			Cookies.set('lang', initData.config.defaultLocale);
		}
		$scope.global.app.locale = Cookies.get('lang');
		
		/*
		* Init some util functions to be used client-side.
		* */
		$scope.global.utils = {
			
			/*
			* Expose the error translation for globally used
			* */
			errors: initData.errors,
			
			/*
			* Check whether a SPA path match a location, used to check for active li when navigating between views.
			* */
			isPath: function (path) {
				return document.location.hash && document.location.hash.indexOf(path) === 2;
			},
			
			/*
			* Cleave options for cleave inputs
			* */
			cleaveOptions: {
				currency: {
					numeral: true,
					numeralDecimalScale: 7
				}
			},
			
			/*
			* Wrap a asyncFunction inside a normal function
			* */
			promisify: function (asyncFunc) {
				try {
					asyncFunc().then(
						() => {
						},
						(err) => {
							throw err;
						}
					);
				}
				catch (err) {
					console.log(err);
				}
			},
			
			/*
			* Escape all regex special characters
			* */
			regexEscape: function (exp) {
				
				return String(exp).replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
			},
			
			/*
			* Break the queries at client-side. This is used to check for any query that has been passed from one route to an other.
			* E.g: /#/user-info?id=xxx&name=zzz
			* */
			breakQueries: function (path) {
				//-- find the ?
				let result = {};
				if (path) {
					let index = path.indexOf('?');
					if (index >= 0) {
						let subPath = path.substr(index + 1);
						//-- then split by &
						let splits = subPath.split('&');
						if (splits && splits.length) {
							splits.forEach(function (pair) {
								//-- finally split by =
								let values = pair.split('=');
								result[values[0]] = values[1];
							});
						}
					}
				}
				return result;
			},
			
			post: async function (url, data) {
				data.token = $scope.global.user.token.access_token;
				return new Promise(function (resolve) {
					$http.post(url, data).then(
						function (response) {
							resolve(response.data);
						},
						function (err) {
							console.log(err);
							resolve({success: false, error: {errorName: 'NETWORK_ERROR'}});
						}
					);
				});
			},
		};
		
		/*
		* init goole gapi as we use google to login/signup
		* */
		gapi.load('auth2', function () {
			gapi.auth2.init();
		});
	}
}]);


/*
* An directive that helps determining whether an image has been loaded or failed to load.
* */
app.directive('imageOnLoad', function () {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			element.bind('load', function () {
				scope.$apply(attrs.imageOnLoad);
			});
			element.bind('error', function () {
				scope.$apply(attrs.imageOnLoad);
			});
		}
	};
});
