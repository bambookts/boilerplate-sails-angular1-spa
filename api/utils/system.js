const bcrypt = require('bcrypt');

const sysUtils = {
	returnError: function (error) {
		
		return {success: false, error: error};
	},
	
	returnSuccess: function (result) {
		if (result && typeof (result) !== "object") console.log('Warning: returnSuccess result should be of type Object');
		return {success: true, result: result};
	},
	
	promisify: function (asyncFunc) {
		return new Promise((resolve, reject) => {
			asyncFunc().then(
				result => {
					if (!result.success)
						return reject(result);
					return resolve(result);
				},
				(err) => {
					console.log(err);
					reject(sysUtils.returnError(Errors.SYSTEM_ERROR));
				}
			);
		});
	},
	
	regexEscape: function (exp) {
		
		if (!exp) return '';
		return String(exp).replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
	},
	
	hash: function (inputPassword) {
		return new Promise(function (resolve, reject) {
			bcrypt.genSalt(sails.config.SALT_ROUND, function (err, salt) {
				if (err) {
					reject(err);
				}
				else {
					bcrypt.hash(inputPassword, salt, function (err, hash) {
						resolve(hash);
					});
				}
			});
		});
	},
	
	compare: function (inputPassword, hash) {
		return new Promise(function (resolve, reject) {
			bcrypt.compare(inputPassword, hash, (err, result) => {
				if (err) {
					reject(err);
				}
				else {
					resolve(result);
				}
			});
		});
	},
	
};

module.exports = sysUtils;
