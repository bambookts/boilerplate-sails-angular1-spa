
const mongoose = require('mongoose');

const constants = {

	AuthMethod: {
		AUTH_PASSWORD: 'AUTH_PASSWORD',
		AUTH_OAUTH: 'AUTH_OAUTH',
	}
};

const authSchema = new mongoose.Schema({
    userID: {type: mongoose.Schema.ObjectId, ref: 'User', required: true},
    authMethod: {type: String, required: true, default: constants.AuthMethod.AUTH_PASSWORD},
    extra1: {type: String},
    extra2: {type: String},
    extra3: {type: String},
});

module.exports = mongoose.model('Auth', authSchema, 'auth');
module.exports.constants = constants;
