module.exports.routes = {


    /*
    * SPA VIEWS SERVING
    *
    * These routes below are static view serving routes, MUST be prefixed by '/views'.
    * To populate the view, add one action inside 'ViewController'.
    * 'root' is the view that is used as Angular SPA main view.
    * 'index' is the view that will be include inside root view's <ng-view> element.
    *
    * NOTE:
    * - these views are populate by template engine, so 'extends', 'include' and other template engine logic are available.
    * - controller action should be named after the path to the serving view in camel-case style.
    * */
    'GET /': {controller: 'ViewController', action: 'getRoot'},
    'GET /views/index': {view: 'index'},
    'GET /views/home/page1': {view: 'home/page1'},
    'GET /views/home/page2': {view: 'home/page2'},
    'GET /views/home/more/page1': {view: 'home/more/page1'},
    'GET /views/home/more/page2': {view: 'home/more/page2'},
    'GET /views/user/index': {view: 'user/index'},


    /*
    * NORMAL VIEWS SERVING
    *
    * This single page application can also serve normal routes like login, logout, signup, ...
    * These controller actions act like normal MVC pattern: the controller is called, logic is processed, and then view is returned.
    * */
    //-- logic serving
    'GET /login': {controller: 'UserController', action: 'getLogin'},
    'GET /logout': {controller: 'UserController', action: 'getLogout'},
    'GET /signup': {controller: 'UserController', action: 'getSignUp'},


    /*
    * API ROUTES
    *
    * Trivial POST API call to system can be achieve by using angular $http module.
    * */
    'POST /login': {controller: 'UserController', action: 'postLogin'},
    'POST /signup': {controller: 'UserController', action: 'postSignUp'},
    'GET /verify-email': {controller: 'UserController', action: 'getVerifyEmail'},

    /*
    * TOKEN-BASED API ROUTES
    *
    * All token-based api required a token issued by an authorization server (AS) to access resource in the resource server (RS).
    * In this case, both AS and RS are on a same server. However, these api routes can be grouped and deployed into an separated API server,
    * along with BatchController and other relevant policies.
    * */
    'POST /api/*' : {policy: 'hasToken'},
    'POST /api/rpc' : {controller: 'BatchController', action: 'rpc'},
    'POST /api/batch' : {controller: 'BatchController', action : 'batch'},
};
